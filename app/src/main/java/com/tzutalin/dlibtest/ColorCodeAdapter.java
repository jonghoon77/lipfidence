package com.tzutalin.dlibtest;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;

import java.util.ArrayList;

public class ColorCodeAdapter extends RecyclerView.Adapter<ColorCodeAdapter.ItemViewHolder> {

    private Context mContext;
    private ArrayList<ColorCodeData> mColorCodes;
    private ColorCodeAdapterCallback mColorCodeAdapterCallback;
    private ArrayList<Integer> mCheckedCheckBoxIndices;

    public ColorCodeAdapter(Context context) {
        mContext = context;

        mColorCodes = Utils.colorList;
        mCheckedCheckBoxIndices = new ArrayList<>();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_color_code, parent, false);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemViewHolder holder, final int position) {
        final ColorCodeData color = mColorCodes.get(position);

        holder.mFlLayout.setBackgroundColor(Color.parseColor(color.getCode()));
        holder.mFlLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mColorCodeAdapterCallback.setColor(color);
            }
        });

        if (mCheckedCheckBoxIndices.contains(position))
            holder.mPickCheckBox.setChecked(true);
        else
            holder.mPickCheckBox.setChecked(false);

        holder.mPickCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mPickCheckBox.isChecked()) {
                    mColorCodeAdapterCallback.addColor(color);
                    mCheckedCheckBoxIndices.add(position);

                } else {
                    mColorCodeAdapterCallback.removeColor(color);
                    mCheckedCheckBoxIndices.remove(mCheckedCheckBoxIndices.indexOf(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mColorCodes.size();
    }

    public void setCallback(ColorCodeAdapterCallback colorCodeAdapterCallback) {
        mColorCodeAdapterCallback = colorCodeAdapterCallback;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private FrameLayout mFlLayout;
        private CheckBox mPickCheckBox;

        ItemViewHolder(View itemView) {
            super(itemView);

            mFlLayout = itemView.findViewById(R.id.adapter_color_code_fl_layout);
            mPickCheckBox = itemView.findViewById(R.id.adapter_color_code_checkbox);
        }
    }

    interface ColorCodeAdapterCallback {
        void addColor(ColorCodeData color);

        void removeColor(ColorCodeData color);

        void setColor(ColorCodeData color);
    }
}
