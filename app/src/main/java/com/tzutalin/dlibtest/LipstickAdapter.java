package com.tzutalin.dlibtest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class LipstickAdapter extends RecyclerView.Adapter<LipstickAdapter.LipstickHolder> {

    private ArrayList<LipstickData> mLipstickList;
    private Context mContext;

    public LipstickAdapter(Context context, ArrayList<LipstickData> lipstickDataArrayList){
        this.mLipstickList = lipstickDataArrayList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public LipstickHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_lipsticks, parent, false);

        return new LipstickHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LipstickHolder holder, final int position) {

        holder.ivLipstick.setBackground(mContext.getResources().getDrawable(mLipstickList.get(position).getImageName()));
        holder.ivLipstick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CameraActivity) mContext).setColorCode(mLipstickList.get(position).getColorCode());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mLipstickList.size();
    }

    public class LipstickHolder extends RecyclerView.ViewHolder{

        ImageView ivLipstick;

        public LipstickHolder(View itemView) {
            super(itemView);

            ivLipstick = itemView.findViewById(R.id.iv_lipstick);
        }
    }
}
