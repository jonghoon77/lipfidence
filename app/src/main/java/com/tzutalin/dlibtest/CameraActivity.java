/*
 * Copyright 2016-present Tzutalin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tzutalin.dlibtest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by darrenl on 2016/5/20.
 */
public class CameraActivity extends Activity {


    private static int OVERLAY_PERMISSION_REQ_CODE = 1;

    private String mCategory;

    private SeekBar mSbOpacityBar;

    private String mBrand = "에뛰드";
    private TextView mTvEtude, mTvPeripera;
    private ImageView mIvCaptureButton;

    private RecyclerView mRvLipsticks;
    private ArrayList<LipstickData> mLipsticksList;
    private LipstickAdapter mLipstickAdapter;

    private CameraConnectionFragment mFragment;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_camera);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this.getApplicationContext())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
            }
        }

        mCategory = getIntent().getStringExtra("category");

        mRvLipsticks = findViewById(R.id.activity_camera_rv_lipsticks);

        mLipsticksList = Utils.getLipstickData(mCategory, mBrand);
        mLipstickAdapter = new LipstickAdapter(CameraActivity.this, mLipsticksList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CameraActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRvLipsticks.setLayoutManager(linearLayoutManager);
        mRvLipsticks.setAdapter(mLipstickAdapter);

        mTvEtude = findViewById(R.id.activity_camera_tv_etude);
        mTvPeripera = findViewById(R.id.activity_camera_tv_peripera);
        mTvEtude.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLipsticksList = Utils.getLipstickData(mCategory, "에뛰드");
                mLipstickAdapter = new LipstickAdapter(CameraActivity.this, mLipsticksList);
                mRvLipsticks.setAdapter(mLipstickAdapter);
            }
        });

        mTvPeripera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLipsticksList = Utils.getLipstickData(mCategory, "페리페라");
                mLipstickAdapter = new LipstickAdapter(CameraActivity.this, mLipsticksList);
                mRvLipsticks.setAdapter(mLipstickAdapter);
            }
        });

        mIvCaptureButton = findViewById(R.id.activity_camera_iv_capture);
        mIvCaptureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap capturedImage = mFragment.getCameraImage();

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                capturedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                Intent intent = new Intent(CameraActivity.this, ResultActivity.class);
                intent.putExtra("data", byteArray);
                startActivity(intent);
            }
        });

        if (null == savedInstanceState) {

            mFragment = CameraConnectionFragment.newInstance();
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, mFragment)
                    .commit();
        }

        mSbOpacityBar = findViewById(R.id.activity_camera_sb_opacity);
        mSbOpacityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mFragment.setAlpha(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void setColorCode(String colorCode){
        mFragment.setColorCode(colorCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this.getApplicationContext())) {
                    Toast.makeText(CameraActivity.this, "CameraActivity\", \"SYSTEM_ALERT_WINDOW, permission not granted...", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            }
        }
    }
}