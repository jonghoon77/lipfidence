package com.tzutalin.dlibtest;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ResultActivity extends AppCompatActivity {

    private String mImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        ImageView ivResultImage = findViewById(R.id.activity_result_iv_captured_image);
        ImageView ivKakaoShare = findViewById(R.id.activity_result_iv_kakao_share);
        ImageView ivSave = findViewById(R.id.activity_result_iv_save);

        byte[] byteArray = getIntent().getByteArrayExtra("data");
        final Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        ivResultImage.setImageBitmap(bitmap);

        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveCapturedImage(bitmap);
            }
        });

        ivKakaoShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mImagePath != null){
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/jpeg");
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mImagePath));
                    intent.setPackage("com.kakao.talk");
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "공유하려면 먼저 사진을 저장해야 합니다", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void saveCapturedImage(Bitmap capturedImage){

        String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String folderName = "/Lipfidence/";
        String fileName = (int)(Math.random() * 10000) + ".jpg";
        String stringPath = storagePath + folderName;

        File file;
        file = new File(stringPath);
        if (!file.isDirectory())
            file.mkdirs();
        try {
            FileOutputStream out = new FileOutputStream(stringPath + fileName);
            capturedImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mImagePath = stringPath + fileName;

        Toast.makeText(getApplicationContext(), "사진이 저장되었습니다", Toast.LENGTH_SHORT).show();
    }
}
