package com.tzutalin.dlibtest;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class ColorCameraResultActivity extends AppCompatActivity {

    private RecyclerView mRvColorList;
    private ColorListAdapter mColorListAdapter;
    private ArrayList<ColorCodeData> mColorList;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_camera_result);

        mRvColorList = findViewById(R.id.activity_color_camera_result_rv_color_list);

        String data = getIntent().getStringExtra("data");
        mColorList = new Gson().fromJson(data, new TypeToken<ArrayList<ColorCodeData>>(){}.getType());
        mColorListAdapter = new ColorListAdapter(this, mColorList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRvColorList.setLayoutManager(linearLayoutManager);
        mRvColorList.setAdapter(mColorListAdapter);

        setRankData(mColorList);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setRankData(ArrayList<ColorCodeData> colorCodeData){

        ArrayList<ColorCodeData> springColors = new ArrayList<>();
        ArrayList<ColorCodeData> summerColors = new ArrayList<>();
        ArrayList<ColorCodeData> autumnColors = new ArrayList<>();
        ArrayList<ColorCodeData> winterColors = new ArrayList<>();

        for (ColorCodeData color : colorCodeData){
            if (color.getKind() == Utils.SPRING_WARM)
                springColors.add(color);
            else if (color.getKind() == Utils.SUMMER_COOL)
                summerColors.add(color);
            else if (color.getKind() == Utils.AUTUMN_WARM)
                autumnColors.add(color);
            else
                winterColors.add(color);
        }

        ArrayList<ArrayList<ColorCodeData>> temp = new ArrayList<>();
        temp.add(springColors);
        temp.add(summerColors);
        temp.add(autumnColors);
        temp.add(winterColors);

        int[] tempArray = new int[4];

        for (int i = 0; i < temp.size(); i++){
            int rank = 1;
            for (int j = 0; j < temp.size(); j++){
                if (temp.get(i).size() < temp.get(j).size())
                    rank++;
            }
            tempArray[i] = rank;
        }

        RankData springRank = new RankData(tempArray[0], springColors.size(), "봄   웜");
        RankData summerRank = new RankData(tempArray[1], summerColors.size(), "여름 쿨");
        RankData autumnRank = new RankData(tempArray[2], autumnColors.size(), "가을 웜");
        RankData winterRank = new RankData(tempArray[3], winterColors.size(), "겨울 쿨");

        final ArrayList<RankData> rankArray = new ArrayList<>();
        rankArray.add(springRank);
        rankArray.add(summerRank);
        rankArray.add(autumnRank);
        rankArray.add(winterRank);


        Collections.sort(rankArray, new Comparator<RankData>() {
            @Override
            public int compare(RankData o1, RankData o2) {
                return o1.getRank() - o2.getRank();
            }
        });

        FrameLayout flRankLayout1 = findViewById(R.id.rank_fl_layout_1);
        FrameLayout flRankLayout2 = findViewById(R.id.rank_fl_layout_2);
        FrameLayout flRankLayout3 = findViewById(R.id.rank_fl_layout_3);
        FrameLayout flRankLayout4 = findViewById(R.id.rank_fl_layout_4);

        flRankLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ColorCameraResultActivity.this, CameraActivity.class);
                intent.putExtra("category", rankArray.get(0).getKind());
                startActivity(intent);
            }
        });
        flRankLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ColorCameraResultActivity.this, CameraActivity.class);
                intent.putExtra("category", rankArray.get(1).getKind());
                startActivity(intent);
            }
        });
        flRankLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ColorCameraResultActivity.this, CameraActivity.class);
                intent.putExtra("category", rankArray.get(2).getKind());
                startActivity(intent);
            }
        });
        flRankLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ColorCameraResultActivity.this, CameraActivity.class);
                intent.putExtra("category", rankArray.get(3).getKind());
                startActivity(intent);
            }
        });

        TextView tvRankNumber1 = findViewById(R.id.rank_tv_number_1);
        tvRankNumber1.setText(String.valueOf(rankArray.get(0).getRank()));
        TextView tvRankNumber2 = findViewById(R.id.rank_tv_number_2);
        tvRankNumber2.setText(String.valueOf(rankArray.get(1).getRank()));
        TextView tvRankNumber3 = findViewById(R.id.rank_tv_number_3);
        tvRankNumber3.setText(String.valueOf(rankArray.get(2).getRank()));
        TextView tvRankNumber4 = findViewById(R.id.rank_tv_number_4);
        tvRankNumber4.setText(String.valueOf(rankArray.get(3).getRank()));

        TextView tvRankColorKind1 = findViewById(R.id.rank_tv_color_kind_1);
        tvRankColorKind1.setText(rankArray.get(0).getKind());
        TextView tvRankColorKind2 = findViewById(R.id.rank_tv_color_kind_2);
        tvRankColorKind2.setText(rankArray.get(1).getKind());
        TextView tvRankColorKind3 = findViewById(R.id.rank_tv_color_kind_3);
        tvRankColorKind3.setText(rankArray.get(2).getKind());
        TextView tvRankColorKind4 = findViewById(R.id.rank_tv_color_kind_4);
        tvRankColorKind4.setText(rankArray.get(3).getKind());

        TextView tvRankColorCount1 = findViewById(R.id.rank_tv_color_count_1);
        tvRankColorCount1.setText(String.valueOf(rankArray.get(0).getCount()));
        TextView tvRankColorCount2 = findViewById(R.id.rank_tv_color_count_2);
        tvRankColorCount2.setText(String.valueOf(rankArray.get(1).getCount()));
        TextView tvRankColorCount3 = findViewById(R.id.rank_tv_color_count_3);
        tvRankColorCount3.setText(String.valueOf(rankArray.get(2).getCount()));
        TextView tvRankColorCount4 = findViewById(R.id.rank_tv_color_count_4);
        tvRankColorCount4.setText(String.valueOf(rankArray.get(3).getCount()));
    }
}
