package com.tzutalin.dlibtest;

import android.widget.LinearLayout;

public class LipstickData {

    public int category;
    public String brand;
    public String name;
    public String colorName;
    public String colorCode;
    public int imageName;

    public LipstickData(int category, String brand, String name, String colorName, String colorCode, int imageName){
        this.category = category;
        this.brand = brand;
        this.name = name;
        this.colorName = colorName;
        this.colorCode = colorCode;
        this.imageName = imageName;
    }

    public int getCategory() {
        return category;
    }

    public String getBrand() {
        return brand;
    }

    public String getName() {
        return name;
    }

    public String getColorName() {
        return colorName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public int getImageName() {
        return imageName;
    }
}
