package com.tzutalin.dlibtest;

public class RankData {

    private int rank;
    private int count;
    private String kind;

    public RankData(int rank, int count, String kind){
        this.rank = rank;
        this.count = count;
        this.kind = kind;
    }

    public int getRank() {
        return rank;
    }

    public int getCount() {
        return count;
    }

    public String getKind() {
        return kind;
    }
}
