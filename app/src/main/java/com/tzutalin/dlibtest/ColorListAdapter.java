package com.tzutalin.dlibtest;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ColorListAdapter extends RecyclerView.Adapter<ColorListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<ColorCodeData> mColorList;

    public ColorListAdapter(Context context, ArrayList<ColorCodeData> colorList)
    {
        mContext = context;
        mColorList = colorList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_color_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ColorCodeData color = mColorList.get(position);

        holder.mFlLayout.setBackgroundColor(Color.parseColor(color.getCode()));

        String kind = null;
        if (color.getKind() == 0)
            kind = "Spring Warm";
        if (color.getKind() == 1)
            kind = "Summer Cool";
        if (color.getKind() == 2)
            kind = "Autumn Warm";
        if (color.getKind() == 3)
            kind = "Winter Cool";

        holder.mTvColorKind.setText(kind);
        holder.mTvColorCode.setText(color.getCode());
    }

    @Override
    public int getItemCount() {
        return mColorList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        FrameLayout mFlLayout;
        TextView mTvColorCode;
        TextView mTvColorKind;

        public ViewHolder(View itemView) {
            super(itemView);

            mFlLayout = itemView.findViewById(R.id.adapter_color_list_fl_layout);
            mTvColorCode = itemView.findViewById(R.id.adapter_color_list_tv_color_code);
            mTvColorKind = itemView.findViewById(R.id.adapter_color_list_tv_kind);
        }
    }
}
