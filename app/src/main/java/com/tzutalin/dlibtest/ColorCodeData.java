package com.tzutalin.dlibtest;

public class ColorCodeData {

    private int kind;
    private String code;


    public ColorCodeData(int kind, String code)
    {
        this.kind = kind;
        this.code = code;
    }

    public int getKind()
    {
        return kind;
    }

    public String getCode()
    {
        return code;
    }
}
