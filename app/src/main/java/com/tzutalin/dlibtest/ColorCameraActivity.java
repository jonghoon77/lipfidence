package com.tzutalin.dlibtest;

import android.Manifest;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

public class ColorCameraActivity extends AppCompatActivity
    implements Camera2Apis.Camera2Interface, TextureView.SurfaceTextureListener {

    private String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    private static final int MULTIPLE_PERMISSIONS = 101;

    private TextureView mPreview;

    private RecyclerView mColors;
    private Camera2Apis mCamera;
    private OverlayColorView mOverlayColorView;
    private FrameLayout mFlLayout;
    private ImageView mIvComplete;

    private ColorCodeAdapter mColorCodeAdapter;
    private ArrayList<ColorCodeData> mClickedColors = new ArrayList<>();
    private ColorCodeAdapter.ColorCodeAdapterCallback colorCodeAdapterCallback = new ColorCodeAdapter.ColorCodeAdapterCallback() {
        @Override
        public void addColor(ColorCodeData color) {
            mClickedColors.add(color);
        }

        @Override
        public void removeColor(ColorCodeData color) {
            mClickedColors.remove(color);
        }

        @Override
        public void setColor(ColorCodeData color) {
            mOverlayColorView.setColorCode(color.getCode());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_camera);

        Utils.initColorData();



        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Toast.makeText(ColorCameraActivity.this, "권한 허가", Toast.LENGTH_SHORT).show();
                mCamera = new Camera2Apis(ColorCameraActivity.this);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(ColorCameraActivity.this, "권한 거부\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setRationaleMessage("사용을 위해서는 권한이 필요합니다")
                .setDeniedMessage("[설정] > [권한] 에서 권한을 허용할 수 있습니다")
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)

                .check();


        mPreview = findViewById(R.id.activity_color_camera_view);
        mPreview.setSurfaceTextureListener(this);
        mColors = findViewById(R.id.activity_color_camera_colors);
        mFlLayout = findViewById(R.id.activity_color_camera_fl_layout);


        mOverlayColorView = findViewById(R.id.activity_color_camera_overlay_view);

        mColorCodeAdapter = new ColorCodeAdapter(this);
        mColorCodeAdapter.setCallback(colorCodeAdapterCallback);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mColors.setLayoutManager(linearLayoutManager);
        mColors.setAdapter(mColorCodeAdapter);

        mIvComplete = findViewById(R.id.activity_color_camera_iv_complete);
        mIvComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ColorCameraActivity.this, ColorCameraResultActivity.class);
                intent.putExtra("data", new Gson().toJson(mClickedColors));
                startActivity(intent);
            }
        });
    }

    private void openCamera() {
        CameraManager cameraManager = mCamera.CameraManager_1(this);
        String cameraId = mCamera.CameraCharacteristics_2(cameraManager);
        mCamera.CameraDevice_3(cameraManager, cameraId);
    }

    @Override
    public void onCameraDeviceOpened(CameraDevice cameraDevice, Size cameraSize) {
        SurfaceTexture texture = mPreview.getSurfaceTexture();
        texture.setDefaultBufferSize(cameraSize.getWidth(), cameraSize.getHeight());
        Surface surface = new Surface(texture);

        mCamera.CaptureSession_4(cameraDevice, surface);
        mCamera.CaptureRequest_5(cameraDevice, surface);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mPreview.isAvailable()) {
            openCamera();
        } else {
            mPreview.setSurfaceTextureListener(this);
        }
    }

    /* Surface Callbacks */
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        if(mCamera!=null) openCamera();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }


    private void closeCamera() {
        mCamera.closeCamera();
    }

    @Override
    protected void onPause() {
        if(mCamera!=null) closeCamera();
        super.onPause();
    }
}
