package com.tzutalin.dlibtest;

import java.util.ArrayList;

public class Utils {

    public static int SPRING_WARM = 0;
    public static int SUMMER_COOL = 1;
    public static int AUTUMN_WARM = 2;
    public static int WINTER_COOL = 3;

    public static ArrayList<ColorCodeData> colorList;


    public static String[] spring_warms =
            {
                    "#f06354", "#ed4242", "#cf3527",
                    "#16a147", "#604d9e", "#f0c348", "#b05927"
            };

    public static String[] summer_cools =
            {
                    "#ffd9ea", "#f06270", "#f7ffbf",
                    "#ccffd4", "#acd8fa", "#d9bef7", "#bec0cc"
            };

    public static String[] autumn_warms =
            {
                    "#8c1717", "#fdbc62", "#ff4500",
                    "#cd5c5c", "#b22222", "#ffa500", "#a52a2a"
            };

    public static String[] winter_cools =
            {
                    "#d97cb2", "#cc2353", "#40d874",
                    "#4052a1", "#f5f4b3", "#ba4788", "#fadce8"
            };

    public static void initColorData()
    {
        if (colorList != null)
            return;

        colorList = new ArrayList<>();

        for (int i = 0; i < spring_warms.length; i++)
        {
            colorList.add(new ColorCodeData(SPRING_WARM, spring_warms[i]));
        }

        for (int i = 0; i < summer_cools.length; i++)
        {
            colorList.add(new ColorCodeData(SUMMER_COOL, summer_cools[i]));
        }

        for (int i = 0; i < autumn_warms.length; i++)
        {
            colorList.add(new ColorCodeData(AUTUMN_WARM, autumn_warms[i]));
        }

        for (int i = 0; i < winter_cools.length; i++)
        {
            colorList.add(new ColorCodeData(WINTER_COOL, winter_cools[i]));
        }
    }

    public static ArrayList<LipstickData> getLipstickData(String category, String brand){

        ArrayList<LipstickData> lipstickDataList = new ArrayList<>();

        if (category.equals("봄   웜")){
            if (brand.equals("페리페라")){
                lipstickDataList.add(new LipstickData(SPRING_WARM, "페리페라",
                        "잉크 더 에어리 벨벳", "7호 나님의 템", "#de4e52", R.drawable.spring1));
                lipstickDataList.add(new LipstickData(SPRING_WARM, "페리페라",
                        "페리스 잉크 더 벨벳", "14호 외모성수기", "#fa6664", R.drawable.spring2));
                lipstickDataList.add(new LipstickData(SPRING_WARM, "페리페라",
                        "페리스 잉크 더 벨벳", "6호 여주인공", "#d94c66", R.drawable.spring3));
            } else {
                lipstickDataList.add(new LipstickData(SPRING_WARM, "에뛰드",
                        "미니 투 매치", "RD303 찰토마토", "#e6322c", R.drawable.spring4));
                lipstickDataList.add(new LipstickData(SPRING_WARM, "에뛰드",
                        "미니 투 매치", "OR201 살몬키스", "#fa8b78", R.drawable.spring5));
                lipstickDataList.add(new LipstickData(SPRING_WARM, "애뛰드",
                        "미니 투 매치", "OR202 슬로우코랄", "#eb706a", R.drawable.spring6));
            }
        } else if (category.equals("여름 쿨")){
            if (brand.equals("페리페라")){
                lipstickDataList.add(new LipstickData(SUMMER_COOL, "페리페라",
                        "잉크 더 에어리 벨벳", "4호 미모열일", "#fd1159", R.drawable.summer1));
                lipstickDataList.add(new LipstickData(SUMMER_COOL, "페리페라",
                        "잉크 더 포근 벨벳", "4호 미모치트키", "#e234a7", R.drawable.summer2));
                lipstickDataList.add(new LipstickData(SUMMER_COOL, "페리페라",
                        "잉크 오리지널", "1호 신의한수", "#f2569a", R.drawable.summer3));
            } else {
                lipstickDataList.add(new LipstickData(SUMMER_COOL, "에뛰드",
                        "디어 마이 블루밍 립스 톡 쉬폰", "PK027 설레이고 쉬폰 핑크", "#f53b53",  R.drawable.summer4));
                lipstickDataList.add(new LipstickData(SUMMER_COOL, "에뛰드",
                        "매트 시크 립 라커", "PK003 예리한 핑크", "#eb313f", R.drawable.summer5));
                lipstickDataList.add(new LipstickData(SUMMER_COOL, "에뛰드",
                        "매트 시크 립 라커", "PK001 어제보다 로제", "#dc474b", R.drawable.summer6));
            }

        } else if (category.equals("가을 웜")){
            if (brand.equals("페리페라")){
                lipstickDataList.add(new LipstickData(AUTUMN_WARM, "페리페라",
                        "잉크 더 에어리 벨벳", "8호 비주얼 깡패", "#a63a2e",  R.drawable.autumn1));
                lipstickDataList.add(new LipstickData(AUTUMN_WARM, "페리페라",
                        "잉크 더 에어리 벨벳", "6호 인생잇템", "#a1240e", R.drawable.autumn2));
                lipstickDataList.add(new LipstickData(AUTUMN_WARM, "페리페라",
                        "잉크 더 에어리 벨벳", "5호 잉크튜드", "#9c2125", R.drawable.autumn3));
            } else {
                lipstickDataList.add(new LipstickData(AUTUMN_WARM, "에뛰드",
                        "디어 마이 블루밍 립스 톡 쉬폰", "BE109 떨리고 쉬폰 베이지", "#73251a",  R.drawable.autumn4));
                lipstickDataList.add(new LipstickData(AUTUMN_WARM, "에뛰드",
                        "로지 틴트 립스", "7호 티 로즈", "#de3633", R.drawable.autumn5));
                lipstickDataList.add(new LipstickData(AUTUMN_WARM, "에뛰드",
                        "매트 시크 립 라커", "RD301 슬기로운 버건디", "#701420",  R.drawable.autumn6));
            }
        } else if (category.equals("겨울 쿨")){
            if (brand.equals("페리페라")){
                lipstickDataList.add(new LipstickData(WINTER_COOL, "페리페라",
                        "잉크더 에어리 벨벳", "9호 최적미모", "#d65156", R.drawable.winter1));
                lipstickDataList.add(new LipstickData(WINTER_COOL, "페리페라",
                        "페리스 잉크 더 벨벳", "9호 남심저격", "#e61c1f", R.drawable.winter2));
                lipstickDataList.add(new LipstickData(WINTER_COOL, "페리페라",
                        "페리스 잉크 더 벨벳", "7호 아련폭팔", "#ed5362", R.drawable.winter3));
            } else {
                lipstickDataList.add(new LipstickData(WINTER_COOL, "에뛰드",
                        "매트 시크 립라커", "BR401 오늘은 웬디브라운", "#a33c43", R.drawable.winter4));
                lipstickDataList.add(new LipstickData(WINTER_COOL, "에뛰드",
                        "디어달링워터젤틴트", "RD305 대추레드", "#b52d44", R.drawable.winter5));
                lipstickDataList.add(new LipstickData(WINTER_COOL, "에뛰드",
                        "디어 마이 블루밍 립스톡 쉬폰", "RD313 예쁘고 쉬폰 레드", "#b81f3e", R.drawable.winter6));
            }
        }

        return lipstickDataList;
    }
}
