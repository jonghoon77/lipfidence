package com.tzutalin.dlibtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class OverlayColorView extends LinearLayout {

    private Bitmap mBitmap;
    private String mCurrentColorCode = "#f06354";

    public OverlayColorView(Context context) {
        super(context);
    }

    public OverlayColorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        if (mBitmap == null)
            createWindowFrame();
        canvas.drawBitmap(mBitmap, 0, 0, null);
    }

    protected void createWindowFrame() {

        mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mBitmap);

        RectF outerRectangle = new RectF(0, 0, getWidth(), getHeight());

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.parseColor(mCurrentColorCode));
        canvas.drawRect(outerRectangle, paint);

        paint.setColor(Color.TRANSPARENT);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
        float centerX = getWidth() / 2;
        float centerY = getHeight() / 2;
        canvas.drawOval(centerX - 160, centerY - 200, centerX + 160, centerY + 200,paint);
    }

    public void setColorCode(String colorCode)
    {
        mCurrentColorCode = colorCode;
        createWindowFrame();
        invalidate();
    }
}
